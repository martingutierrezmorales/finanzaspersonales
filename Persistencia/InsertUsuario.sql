DELIMITER //
-- DROP PROCEDURE InsertUsuario
DROP PROCEDURE IF EXISTS InsertUsuario;

CREATE PROCEDURE InsertUsuario(IN p_usuario VARCHAR(40), 
							   IN p_nombre VARCHAR(50), 
							   IN p_password VARCHAR(80), 
							   IN p_tipo_usuario INT(11), 
							   OUT result_msg VARCHAR(30))
BEGIN
    DECLARE usuarioExistente INT;

    -- Verifica si el usuario ya existe
    SELECT COUNT(*) INTO usuarioExistente FROM usuarios WHERE usuario = p_usuario;

    IF usuarioExistente = 0 THEN
        -- Si el usuario no existe, inserta el nuevo usuario
        INSERT INTO usuarios(usuario, nombre, `password`, tipo_usuario)
        VALUES (p_usuario, p_nombre, p_password, p_tipo_usuario);
        
        SET result_msg = 'Usuario insertado exitosamente.';
    ELSE
        -- Si el usuario ya existe, no inserta y envía un mensaje
        SET result_msg = 'El usuario ya existe.';
    END IF;

END //

DELIMITER ;