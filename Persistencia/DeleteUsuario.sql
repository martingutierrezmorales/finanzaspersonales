DELIMITER //

DROP PROCEDURE IF EXISTS DeleteUsuario;
/*
CALL DeleteUsuario('leslylit',@result_msg)
select @result_msg
*/
CREATE PROCEDURE DeleteUsuario(IN p_usuario VARCHAR(40), OUT result_msg VARCHAR(255))
BEGIN
    -- Verifica si el usuario existe
    DECLARE usuarioExistente INT;

    SELECT COUNT(*) INTO usuarioExistente FROM usuarios WHERE usuario = p_usuario;

    IF usuarioExistente = 1 THEN
        -- Si el usuario existe, elimínalo
        DELETE FROM usuarios WHERE usuario = p_usuario;
        SET result_msg = 'Usuario eliminado exitosamente.';
    ELSE
        -- Si el usuario no existe, envía un mensaje
        SET result_msg = 'Usuario no encontrado.';
    END IF;
END //

DELIMITER ;
