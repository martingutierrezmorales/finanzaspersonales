USE Contabilidad
/*Cat Tipo de Pago*/

CREATE TABLE LKP_TipoPago (
    id INT AUTO_INCREMENT PRIMARY KEY,
    descripcion VARCHAR(50) NOT NULL,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO LKP_TipoPago(descripcion)
VALUES('Quincena'),
('Catorcena');

SELECT * FROM `LKP_TipoPago` WHERE 1

/*Cat Empresa */

CREATE TABLE LKP_Empresa (
    id INT AUTO_INCREMENT PRIMARY KEY,
    descripcion VARCHAR(50) NOT NULL,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO LKP_Empresa(descripcion)
VALUES('Magnicharters'),
('DHL'),
('Bursamerica'),
('PanAmericano'),
('El Aguila'),
('TasiSoft');

-- Añadir el campo de Fecha de Ingreso
ALTER TABLE LKP_Empresa
ADD fecha_ingreso DATE;

-- Añadir el campo de Fecha de Despido
ALTER TABLE LKP_Empresa
ADD fecha_despido DATE;

SELECT * FROM `LKP_Empresa` WHERE 1

/*Catalogo Ingreso */

CREATE TABLE LKP_Ingreso (
    id INT AUTO_INCREMENT PRIMARY KEY,
    descripcion VARCHAR(50) NOT NULL,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO LKP_Ingreso(descripcion)
VALUES('Despensa'),
('Nomina'),
('Trabajo Extra'),
('Me Debian'),
('Me Lo Encontre');

SELECT * FROM `LKP_Ingreso` WHERE 1


/*Catalogo Egreso */

CREATE TABLE LKP_Egreso (
    id INT AUTO_INCREMENT PRIMARY KEY,
    descripcion VARCHAR(50) NOT NULL,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO LKP_Egreso(descripcion)
VALUES('Luz'),
('Telefono'),
('Despensa'),
('Colegiatura Lesly'),
('Colegiatura Israel');

SELECT * FROM `LKP_Egreso` WHERE 1

