DELIMITER //

DROP PROCEDURE IF EXISTS GetUsuarios;

CREATE PROCEDURE GetUsuarios(IN p_usuario VARCHAR(10))
BEGIN

    IF TRIM(p_usuario) <> '0' THEN
        SELECT id, usuario, nombre,`password`, tipo_usuario 
        FROM usuarios 
        WHERE usuario = p_usuario;
    ELSE 
        SELECT id, usuario, nombre,`password`, tipo_usuario 
        FROM usuarios;
    END IF;

END //

DELIMITER ;
