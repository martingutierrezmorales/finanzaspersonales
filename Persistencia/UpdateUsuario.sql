DELIMITER //
/*
CALL UpdateUsuario('martingm', 'Rogelio Luna', 'Burrito', '3')
*/
DROP PROCEDURE IF EXISTS UpdateUsuario;

CREATE PROCEDURE UpdateUsuario( IN p_usuario VARCHAR(40), 
                               IN p_nombre VARCHAR(80), 
                               IN p_password VARCHAR(50), 
                               IN p_tipo_usuario INT(11))
BEGIN
    

        -- Actualiza el usuario basado en el usuario
        UPDATE usuarios
        SET nombre = p_nombre,
            `password` = SHA1(p_password),
            tipo_usuario = p_tipo_usuario
        WHERE usuario = p_usuario;

END //

DELIMITER ;