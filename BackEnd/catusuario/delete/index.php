<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once "../../dtos/catusuario.php";
    
    if ($_SERVER["REQUEST_METHOD"] == "DELETE") {
        // Obtener datos POST
        $data = json_decode(file_get_contents("php://input"));
        $user = new User();

        if(isset($data->usuario)){
        $result = $user->deleteUsuario($data->usuario);
            echo json_encode(["message"=> $result]);
        } else{
            echo json_encode(["message" => "El Usuario no pudo ser borrado"]);
        }

    }else{
        echo json_encode(["message" =>  $_SERVER["REQUEST_METHOD"]]);
    }
    
    
    
?>