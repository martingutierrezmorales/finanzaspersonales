<?php

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once "../../dtos/catusuario.php";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

// Obtener datos POST
        $data = json_decode(file_get_contents("php://input"));
        $user = new User();

        if (isset($data->usuario) && isset($data->nombre) && isset($data->password) && isset($data->tipo_usuario)) {
            
            $result = $user->insertacatusuario($data->usuario, $data->nombre, $data->password, $data->tipo_usuario);
            echo json_encode(["message" => $result]);
            /*
            if ($result) {
                echo json_encode(["message" => "Usuario insertado exitosamente"]);
            } else {
                
               
                echo json_encode(["message" => "Error al insertar usuario"]);
            }
            */
        } else {
            echo json_encode(["message" => "Datos incompletos"]);
        }
        
    }else{
        echo json_encode(["message" =>  $_SERVER["REQUEST_METHOD"]]);
    }



?>
