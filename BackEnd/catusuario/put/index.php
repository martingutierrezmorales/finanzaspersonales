<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once "../../dtos/catusuario.php";

    if ($_SERVER["REQUEST_METHOD"] == "PUT") {

// Obtener datos POST
        $data = json_decode(file_get_contents("php://input"));
        $user = new User();

        if (isset($data->usuario) && isset($data->nombre) && isset($data->password) && isset($data->tipo_usuario)) {
            
            $result = $user->updatescatusuario($data->usuario, $data->nombre, $data->password, $data->tipo_usuario);
            
            if ($result) {
                echo json_encode(["message" => "Usuario Actualizado exitosamente"]);
            } else {
                echo json_encode(["message" => $result]);
                //echo json_encode(["message" => "Error al Actualizar usuario"]);
            }
        } else {
            echo json_encode(["message" => "Datos incompletos"]);
        }
        
    }else{
        echo json_encode(["message" =>  $_SERVER["REQUEST_METHOD"]]);
    }



?>
