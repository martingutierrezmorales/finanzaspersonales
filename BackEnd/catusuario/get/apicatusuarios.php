<?php

include_once '../../dtos/catusuario.php';

class ApiUser{


    function    getAll(){
        $user = new User();
        $users = array();
        $users["catusuarios"] = array();

        $res = $user->obtenerUser();

        if($res->rowCount()){
            while ($row = $res->fetch(PDO::FETCH_ASSOC)){
    
                $item=array(
                    "id" => $row['id'],
                    "usuario" => $row['usuario'] ?? '',
                    "password" => $row['password'],
                    "nombre" => $row['nombre'],
                    "tipo_usuario"=> $row['tipo_usuario'],
                );
                array_push($users["catusuarios"], $item);
            }
        
            echo json_encode($users);
        }else{
            echo json_encode(array('mensaje' => 'No hay elementos'));
        }
    }

    function getUser($usuario2){
         $user = new User();
        $users = array();
        $users["catusuarios"] = array();

        $res = $user->obtenerporUser($usuario2);

        if($res->rowCount()){
            while ($row = $res->fetch(PDO::FETCH_ASSOC)){
    
                $item=array(
                    "id" => $row['id'],
                    "usuario" => $row['usuario'] ?? '',
                    "password" => $row['password'],
                    "nombre" => $row['nombre'],
                    "tipo_usuario"=> $row['tipo_usuario'],
                );
                array_push($users["catusuarios"], $item);
            }
        
            echo json_encode($users);
        }else{
            echo json_encode(array('mensaje' => 'No hay elementos'));
        }

    }
}

?>