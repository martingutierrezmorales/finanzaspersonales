<?php

include_once '../../data/db.php';

class User extends DB{

    function obtenerUser(){
        try{

        $query = $this->connect()->query('CALL GetUsuarios(0)');
       // Devuelve los resultados como un array
        return $query;

        } catch(PDOException $e) {
            // Manejo de errores (puede adaptarse según tus necesidades)
            die("Error: " . $e->getMessage());
        }
    }

    function obtenerporUser($usuario){
        try {
            $query = $this->connect()->prepare("CALL GetUsuarios(?)");
            $query->bindParam(1, $usuario, PDO::PARAM_STR, 10);

            $query->execute();

            return $query;
            
        } catch(PDOException $e) {
            // Manejo de errores (puede adaptarse según tus necesidades)
            die("Error: " . $e->getMessage());
        }
    }

    function insertacatusuario($usuario, $nombre, $password, $tipo_usuario) {

            try{
                $pdo = $this->connect();
                // Definir el parámetro de salida
                $pdo->exec("SET @result_msg = ''");

                $passwordE = password_hash($password, PASSWORD_BCRYPT);
                
            $query = $pdo->prepare("CALL InsertUsuario(?, ?, ?, ?,@result_msg)");
            $query->bindParam(1, $usuario, PDO::PARAM_STR, 40);
            $query->bindParam(2, $nombre, PDO::PARAM_STR, 80);
            $query->bindParam(3, $passwordE, PDO::PARAM_STR, 50);
            $query->bindParam(4, $tipo_usuario, PDO::PARAM_INT, 11);
            $query->execute();
            
            // Obtener el mensaje de resultado
            $result = $pdo->query("SELECT @result_msg AS result_msg")->fetch(PDO::FETCH_ASSOC);
            //$row = $result->fetch(PDO::FETCH_ASSOC);
            $message = $result['result_msg'];
        
            $query = null; 
            
            return $message;

        }catch(PDOException $e){
            echo "Error: " . $e->getMessage();
        }
      
    }

    function updatescatusuario($usuario, $nombre, $password, $tipo_usuario) {

            try {

                $passwordE = password_hash($password, PASSWORD_BCRYPT);
        
            $query = $this->connect()->prepare("CALL UpdateUsuario(?,?,?,?)");
            $query->bindParam(1, $usuario, PDO::PARAM_STR, 40);
            $query->bindParam(2, $nombre, PDO::PARAM_STR, 80);
            $query->bindParam(3, $passwordE, PDO::PARAM_STR, 50);
            $query->bindParam(4, $tipo_usuario, PDO::PARAM_INT, 11);
            
            if($query->execute()){
                return true;
            }
            return false;

            $query = null;   
        
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }
    
    function deleteUsuario($usuario) {

        try{

            $pdo = $this->connect();
            // Definir el parámetro de salida
            $pdo->exec("SET @result_msg = ''");

            // Llamar al procedimiento almacenado
            $query = $pdo->prepare("CALL DeleteUsuario(?, @result_msg)");
            $query->bindParam(1, $usuario, PDO::PARAM_STR, 40);
            $query->execute();

            // Recuperar el valor del parámetro de salida
            $result = $pdo->query("SELECT @result_msg AS result_msg")->fetch(PDO::FETCH_ASSOC);
            $message = $result['result_msg'];
            $query = null; 
            //echo $message; // Esto imprimirá el mensaje resultante del SP
            return $message;
 
        }catch(PDOException $e){
            echo "Error: " . $e->getMessage();
        }

    }
}

?>